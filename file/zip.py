
import os
import sys
import stat
import shutil
import zipfile
from cStringIO import StringIO


class SimpleZip:
    '''
        jesus.olmos@blueliv.com
        Simple zip and unzip, doesn't support sub-directories
    '''

    def __init__(self):
        self.rm = False
        self.create = False

    def zip(self,directory,filename):
        directory = directory+'/'
        orig = os.getcwd()
        os.chdir(directory)

        z = zipfile.ZipFile(filename,'w')
        try:
            for f in os.listdir('.'):
                z.write(f)
        finally:
            z.close()

        os.chdir(orig)

        if self.rm:
            shutil.rmtree(directory)


    def unzip(self,src,to):
        with zipfile.ZipFile(src) as zf:
            for member in zf.infolist():
                # Path traversal defense copied from
                # http://hg.python.org/cpython/file/tip/Lib/http/server.py#l789
                words = member.filename.split('/')
                path = to
                for word in words[:-1]:
                    drive, word = os.path.splitdrive(word)
                    head, word = os.path.split(word)
                    if word in (os.curdir, os.pardir, ''): continue
                    path = os.path.join(path, word)
                zf.extract(member, path)




if __name__ == '__main__':
    z = SimpleZip()
    #z.zip('/tmp/xx','/tmp/xx.zip')
    #z.unzip('/tmp/xx.zip','/tmp/')



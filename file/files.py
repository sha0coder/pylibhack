import logging

class Files:
    def __init__(self,directory,ext,files):
        for f in files.split():
            try :
                setattr(self,f,open(directory+'/'+f+ext).read())
            except:
                logging.error('cant load file: %s' % f)


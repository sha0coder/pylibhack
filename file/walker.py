'''
	Walker() a directory recursive walker
	by @sha0coder
'''

import os

class Walker:	

	def file(self,filepath):
		return True # override (false to stop)
	
	def dir(self,directory):
		return True # override (false to stop)
		
	def walk(self,path):
		for root, dirs, files in os.walk(path):
			for f in files:
				if not self.file(root+f):
					return

			for d in dirs:
				if not self.dir(root+d):
					return
				self.walk(root+d+'/')


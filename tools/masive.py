#!/usr/bin/env python
import threading
import util.jobs

l = util.jobs.AtomicLifo([1,2,3,4,6,7,8,9,10,11,12,14,15,16])
l.push(17)
l.reverse()

class Node(threading.Thread):
	def run(self):
		item = '1'
		while item != '':
			item = l.pop()
			print item


j = util.jobs.Jobs()
j.add(Node())
j.add(Node())
j.add(Node())

j.start()
j.wait()

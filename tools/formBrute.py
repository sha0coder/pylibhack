#!/usr/bin/env python

import os
os.chdir('..')

import sys
from web.bruter import WebBruter


if len(sys.argv) != 4:
	print "USAGE: ./formBrute.py [url] [post] [badguy]"
	sys.exit(1)


wl = '/home/sha0/audit/wordlists/'

b = WebBruter()

b.load(wl+'abc4')
b.load(wl+'openwall')
b.load(wl+'big_pw')

b.badguy = sys.argv[3]
b.numthreads = 30
b.scan(sys.argv[1],sys.argv[2])
b.join()

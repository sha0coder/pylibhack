'''
	@sha0coder
	HTTP helper
'''

from url import URL
from httplib import HTTPConnection, HTTPSConnection

class HTTP:

	def __init__(self,url,timeout=10,isKeepalive=True):
		self.url = URL(url)
		self.timeout = timeout
		self.trace = False

		self.headers = {
			"Content-type": "application/x-www-form-urlencoded",
			"Accept": "text/plain",
			"User-agent": "Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0",
			"Connection": "keepalive"
		}

		if self.url.getProtocol() == 'https':
			self.conn = HTTPSConnection(self.url.getHost(),timeout=timeout)
			
		elif self.url.getProtocol() == 'http':
			self.conn = HTTPConnection(self.url.getHost(),timeout=timeout)

		else:
			raise Exception("unsuported protocol")

	def __del__(self):
		self.close() # avoid CLOSE_WAIT


	def _send(self,method,url,data=None):
		resp = None
		html = ''
		try:
			if data:
				self.conn.request(method, url, data, self.headers)
			else:
				resp = self.conn.request(method, url, headers=self.headers)

			resp = self.conn.getresponse()
			if self.trace:
				print '[%s] %s' % (resp.status,url)
			html = resp.read()
		finally:
			return resp, html

	def head(self,url):
		return self._send("HEAD",url)

	def get(self,url):
		return self._send("GET",url)

	def post(self,url,data):
		return self._send("POST",url,data)
		

	def close(self):
		try:
			self.conn.close()
		except:
			pass
		#TODO: hack the socket




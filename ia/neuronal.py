
import sys
from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import TanhLayer
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer


ds = SupervisedDataSet(1, 1)
'''
ds.addSample((1,4,2,4,3,3,4), (ord('a'),))
ds.addSample((4,1,5,5,3,1,1), (ord('t'),))
ds.addSample((3,4,4,2,4,4,3), (ord('g'),))
ds.addSample((2,1,4,1,4,5,4), (ord('e'),))
ds.addSample((1,4,3,2,4,4,2), (ord('i'),))

ds.addSample((4,5,2,2,4,2,5), (ord('a'),))
ds.addSample((3,4,4,4,3,2,1), (ord('s'),))
'''
ds.addSample((82250), (105))
ds.addSample((81346), (97))
ds.addSample((202075), (116))
ds.addSample((176859), (103))
ds.addSample((106666), (101))

net = buildNetwork(1, 5, 1, bias=True)
trainer = BackpropTrainer(net, ds)
print trainer.train()
#print trainer.trainUntilConvergence()

print net.activate((252333))

'''
print chr(net.activate((5,2,2,4,1,1,3)))
print chr(net.activate((3,4,4,4,3,2,1)))
print chr(net.activate((4,5,2,2,4,2,5)))
'''

a=[0x14,0x32,0x44,0x25,0x22,0x41,0x13,0x14,0x24,0x33,0x43,0x44,0x43,0x21,0x41,0x55,0x31,0x14,0x52,0x24,0x25,0x34,0x42,0x44,0x32,0x14,0x14,0x54]
b=[1432442, 5224113, 1424334, 3444321, 4155311, 4522425, 3442443, 2141454]

connection_rate = 1
learning_rate = 0.7
num_input = 7
num_neurons_hidden = 1
num_output = 1

desired_error = 0.0001
max_iterations = 10 #0000
iterations_between_reports = 1000



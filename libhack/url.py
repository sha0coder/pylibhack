#!/usr/bin/env python2
'''
	URL() url parsing by @sha0coder
	v1.2
'''


import re
import socket
import httplib2
from httplib2 import Http

class URL:

	def __init__(self,url=None):
		self.headers = {
			"Content-type": "application/x-www-form-urlencoded",
			"Accept": "text/plain",
			"User-agent": "Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0"
		}
		self.url = url
		self.base = ''
		self.proto = 'http'
		self.ssl = False
		self.hostport = ''
		self.host = ''
		self.port = 80
		self.path = '/'
		self.params = ''
		self.ip = None

		if url:
			self.parse(url)

	def __str__(self):
		out  = 'url: %s\n' % self.url       #TODO: mejor con una estructura
		if self.ip:
			out += 'ip: %s\n' % self.ip
		out += 'base: %s\n' % self.base
		out += 'proto: %s\n' % self.proto
		out += 'ssl: %s\n' % self.ssl
		out += 'hostport: %s\n' % self.hostport
		out += 'host: %s\n' % self.host
		out += 'port: %s\n' % self.port
		out += 'path: %s\n' % self.path
		out += 'params: %s\n' % self.params
		out += '---'
		return out


	def getUrl(self):
		return self.url
	def getBase(self):
		return self.base
	def isSSL(self):
		return self.ssl
	def getProtocol(self):
		return self.proto
	def getHost(self):
		return self.host
	def getDomain(self):
		return self.host
	def getPort(self):
		return self.port
	def getPath(self):
		return self.path
	def getParams(self):
		return self.params

	def resolve(self,name):
		try:
			return socket.gethostbyname(name)
		except:
			return ''


	def parse(self,url):
		try:
			url = self.fixUrl(url)
			self.url = url
			parts = url.split('/')

			self.proto = parts[0][:-1]
			self.ssl = (self.proto == 'https')
			if self.ssl:
				self.port = 443
			else:
				self.port = 80

			self.hostport = parts[2]
			hostparts = self.hostport.split(':')
			self.host = hostparts[0]
			if len(hostparts) > 1:
				try:
					self.port = int(hostparts[1])
				except:
					print 'bad url '+url
					self.port = 80
			else:
				self.port = 80
				
			self.ip = None
			
			self.base = self.endWithBar('%s://%s' % (self.proto, self.hostport))
			
			if len(self.url)<len(self.base):
				self.url = self.base
			self.path = self.url.replace(self.base,'')
			if self.path == '':
				self.path = '/'
				
				
			parts = self.path.split('?')
			self.path = parts[0]
			if len(parts)>1:
				self.params = parts[1]
			else:
				self.params = ''
				
		except Exception, e:
			print e.message, e.args

	def endswith(self,s):
		return self.url.endswith(s)

	def isDirectory(self):
		return self.url.endswith('/')

	def getBaseDirectory(self):
		url = self.getBase()+self.getDirectory()
		if url[-2:] == '//':
			return url[:-1]
		return url

	

	def getDirectory(self):
		if '/' not in self.path:
			return '/'
		
		ddir = self.endWithBar(re.sub('/[^/]+$','/',self.path))  #TODO: compile
		if ddir[-2:] == '//':
			return ddir[:-1]
		return ddir

	def endWithBar(self,url):
		if url[-1] != '/':
			url += '/'
		return url

	def getIPurl(self):		
		spl = self.getUrl().split('/')
		spl[2] = '%s:%d' % (self.getIP(),int(self.getPort()))
		return '/'.join(spl)
	
	def getIPbase(self):
		spl = self.getBase().split('/')
		spl[2] = '%s:%d' % (self.getIP(),self.getPort())
		return self.endWithBar('/'.join(spl))

	def getIPbaseDirectory(self):
		spl = self.getBaseDirectory().split('/')
		spl[2] = '%s:%d' % (self.getIP(),self.getPort())
		return self.endWithBar('/'.join(spl))
		
	def getIP(self):
		if not self.ip:
			self.ip = self.resolve(self.host)
		return self.ip
	
	def getSubUrls(self):
		return self._getSubUrls(self.path)

	def getUrlDirectory(self):
		return self.getDirectory()

	def _getSubUrls(self,url):
		suburls = []
		try:
			prev = '/'
			url = self.fix(url)[1:]
			
			if '://' in url:
				url = url.split('://')[1]
	
			spl = url.split('/')
			for i in range(0,len(spl)):
				if spl[i] != '':
					prev += spl[i]
	
					if i+1 < len(spl): # not last
						prev += '/'
					
					u = URL(self.base+prev)
					suburls.append(u)
		except:
			print 'error on URL().getSubUrls()'
		
		suburls.reverse()
		return suburls
		
		'''
		for i in range(0,len(spl)):
			if i+1 == len(spl) and spl[i] == '':
				break

			prev += '/'+spl[i]
			if i+1 == len(spl):
				suburls.append(prev)
			else:
				suburls.append(prev+'/')
		'''

		return suburls


	def fix(self,url):
		url = url.replace('\t','')
		url = re.sub('(\?|%3f).*','',url)
		url = url.replace('\x0d','%0d').replace('/./','/').replace('/\.\./','/').replace('#','')
		url = re.sub('//+^:','/',url)

		if '://' not in url:
			if url[0] != '/':
				url = '/'+url
			
		return url




	def getDirBase(self,url=None):
		if not url:
			url = self.url
		return re.sub('/[^/]+$','/',url)


	def fixUrlDir(self,url):
		url = url.replace('\t','')
		url = self.fixUrl(url)
		if url[-1] != '/':
			url += '/'
		return url

	def fixUrl(self,url):
		url = url.replace('\t','')
		url = url.replace('\x0d','%0d').replace('/./','/').replace('/\.\./','/').replace('#','');
		if '://' not in url:
			url = 'http://'+url

		i = url.find('//')            #TODO: optimizar
		i = url.find('//',i+1)
		while i>=0:
			url = url[:i]+url[i+1:]
			i=url.find('//',i+1)
			
		return url

	
	def get(self,timeout=10):
		socket.setdefaulttimeout(timeout)
		http = Http(timeout=timeout)
		try:
			hdrs, html = http.request(self.url, 'GET', headers=self.headers, redirections=0)
			return hdrs, html
		except socket.timeout:
			return None, ''
		except httplib2.RedirectLimit:
			return None, ''
		except Exception,e:
			print e.message
			print e.args
			return None, '' 



	def urls2dir(self,url1,url2):
		'''    
		    Path to go from url1 to url2
		    basic version!!, this only descend directories
		'''
		url1 = self.getDirBase(url1)
		url2 = self.getDirBase(url2)
		
		if url1 == url2:
			return './'
		
		src = URL(url1).path
		dst = URL(url2).path
		
		src = re.sub('//+','/',src)
		dst = re.sub('//+','/',dst)
		
		if src == dst:
			return './'
		
		#maxlen, minlen = __bigger(src,dst)
		
		if src[-1] == '/':
			src = src[:-1]
		if dst[-1] == '/':
			dst = dst[:-1] 
		
		xsrc = src.split('/')
		xdst = dst.split('/')
		
		path = ''
		for i in range(len(xsrc)):
			if len(xdst) <= i:
				path += '../'
			elif xsrc[i] != xdst[i]:
				path += '../'

		return path








def test_suburls():
	u = 'http://pepe.com/lala/lolo/lulu.php?a=1&b=2'
	oUrl = URL(u)
	for u in  oUrl.getSubUrls():
		print u.getUrl()
	

	
	

def massTest():
	testurls = '''
		| http://blessmyhustles.com  |
		| http://stpatrickscorkcu.ie/.root01/gate.php							  |
		| http://youover.biz/forum/gate.php										|
		| http://dada.hanztrading.com/corme/1/cp.php?m=login					   |
		| http://engaugeparts.com.au/components/com_finder/js/gate.php			 |
		| http://descubretuser.com/update/gate.php								 |
		| http://engaugeparts.com.au/components/com_finder/js/cp.php?letter=login  |
		| http://descubretuser.com/update/cp.php?m=login						   |
		| http://178.248.85.67/zeus/gate.php									   |
		| http://www.thecorp.info												  |
		| http://asformations.fr/info/js/cp.php?m=login							|
		| http://aladingetHost-musichall.com/logs/trol/gate.php						   |
		| http://kdp-company.info/office/femi/server/cp.php?m=login				|
		| http://kdp-company.info/office/femi/server/gate.php					  |
		| http://64.37.52.22/~asareus/jar/cp.php?m=login						   |
		| http://higan.org/wp-includes/js/upload/gate.php						  |
		| http://www.866rfgroup.com/wonder/gate.php								|
		| http://fionamcauslan.com/aj/gate.php									 |
		| http://109.229.36.65/zmx/zmx-g.php									   |
		| http://asformations.fr												   |
		| http://94.102.59.204/~user/.joe/cp.php?m=login						   |
		| http://blessmyhustles.com												|
		| http://biotic.ro/direct/.mario/cp.php?m=login							|
		| http://ifrclan.it/images/css/cp.php?m=login							  |
		| http://ifrclan.it/images/css/cp.php?m=login							  |
		| http://www.866rfgroup.com/wonder/cp.php?m=login						  |
		| http://neatnewmanny.co.uk												|
		| http://207.45.188.210/~thepurpl/limpopo/oscar/cp.php?letter=login		|
		| http://207.45.188.210/~thepurpl/limpopo/oscar/cp.php?letter=login		|
		| http://www.thecorp.info/confidential/sorter/gate.php					 |
		| http://adeirmahselviana.com/1/2/cp.php?m=login						   |
		| http://67.228.98.175/~ather/.tmp/gate.php								|
		| http://obutto.eu/shop/admin/index/upload/gate.php						|
		| http://67.228.98.175													 |
		| http://obutto.eu														 |
		| http://metaphororganic.com/wp-includes/jjj/cp.php?m=login				|
		| http://butikbrands.com/image/server/cp.php?m=login					   |
		| http://bashcamron.com/html/cp.php?m=login								|
		| http://sub.begetHostirinckx.be/server/cp.php?m=login							|
		| http://173.249.152.23													|
		| http://carluccigiardina.org/igwe/config.bin							  |
		| http://213.147.67.20													 |
		| http://www.bartollini.pl												 |
		| http://bossmoneytools.biz/images/gps/gate.php							|
		| http://bossmoneytools.biz/images/gps/cp.php?m=login					  |
		| http://ambient-gradnje.si/2/1/config.bin								 |
		| http://empirelightingresources.com/gov/_fresh/cp.php?m=login			 |
		| http://xiistones.com/images/install/cp.php?m=login					   |
		| http://xiistones.com/images/install/cp.php?m=login					   |
		| http://www.vip-file.eu/css/gate.php									  |
		| http://www.vip-file.eu												   |
		| http://infosousahost.com/.media/_pip/cp.php?m=login					  |
		| http://infosousahost.com/.media/_pip/cp.php?m=login					  |
		| http://breconbeaconswedding.com/image/cfg.bin							|
		| http://arnondemello.com.br/cgi/cp.php?m=login							|
		| http://creamlonsarter.co.uk/co/cfg.bin								   |
		| http://creamlonsarter.co.uk/co/gate.php								  |
		| http://ambient-gradnje.si/2/1/cp.php?m=login							 |
		| http://biterelish.co.za/txt/cp.php?m=login							   |
		| http://lfs.ma/apps/films/gate.php										|
		| http://94.242.216.33/cp.php?m=login									  |
		| http://lfs.ma															|
		| http://crowniih.com													  |
		| http://oluwaonpoiny.us												   |
		| http://alliedtrading.net/assets/log/cp.php?m=login					   |
		| http://fasttrackia.pw/metro/admin/1/secure.php						   |
		| http://uncluvmeteam.ru/psi/redir.php									 |
		| http://3dollarlr.com/crip/buchifrankjovia/cache/cent/gate.php			|
		| http://fasttrackia.pw/metro/admin/1/cp.php?letter=login				  |
		| http://www.themmaopen.co.uk/wp-includes/1/cp.php?m=login				 |
		| http://www.navarrainternetmeeting.com/zoom/cp.php?m=login				|
		| http://173.249.152.23													|
		| http://212.44.64.202													 |
		| http://serviskonicaminolta.com/images/book/citadel2/gate.php			 |
		| http://edanurjezi.com/img/colour/etag.php								|
		| http://drcengizcankal.com												|
		| http://solomybro.com/more/moni/metro/admin/1/secure.php				  |
		| http://artskit.in/ven/cp.php?m=login									 |
		| http://bestbuyautotransport.com.au/tree/cp.php?m=login				   |
		| http://perfectwholesaledistributors.com/go/cp.php?m=login				|
		| http://perfectwholesaledistributors.com/go/gate.php					  |
		| http://i-buy.gr/log/cp.php?m=login									   |
		| http://powdereddoughnut.com/image/cp.php?m=login						 |
		| http://chrysoevents.com/news/cp.php?m=login							  |
		| http://142.0.36.226/office/blarry/server/cp.php?m=login				  |
		| http://142.0.36.226/office/david/server/cp.php?m=login				   |
		| http://142.0.36.226/office/isiaka/server/cp.php?m=login				  |
		| http://142.0.36.226/office/nassy/server/cp.php?m=login				   |
		| http://142.0.36.226/office/ebony/server/cp.php?m=login				   |
		| http://142.0.36.226/office/badoo/server/cp.php?m=login				   |
		| http://loveme4u.eu/wp-includes/Text/Diff/Renderer/love.bin			   |
		| http://charglosom.biz/cgi/pan/2015/news/z.php							|
		| http://www.efrelectronics.com.br/images/banner/cp.php?m=login			|
		| http://www.amandapin.com.br/wp-includes/css/style/cp.php?m=login		 |
		| http://www.wgcustom.com/site/wp-admin/css/style/cp.php?m=login		   |
		| http://www.eunaoestouso.com.br/wp-includes/css/style/cp.php?m=login	  |
		| http://212.44.64.202/kec/borodinskoesrajenie.jpg						 |
		| http://87.236.215.82/office/baily/server/cp.php?m=login				  |
		| http://87.236.215.82/office/jona/server/cp.php?m=login				   |
		| http://87.236.215.82/office/zb/adminpanel/admin.php?m=stats_main		 |
		| http://cumfaci.eu/html/gate.php										  |
		| http://177.70.96.129/~souza2/css/gate.php								|
		| http://89.248.161.244/fuck/xren.php?m=login							  |
		| http://177.70.96.129/~souza2/css/cp.php?m=login						  |
		| http://49.50.8.213/~h23912/js/install/cp.php?m=login					 |
		| http://inlandbeardeddragons.com/templates/beez/.ama/cp.php?m=login	   |
		| http://www.canalsaleluz.com.br/wp-includes/css/style/cp.php?m=login	  |
		| http://184.173.68.2/~jacob911/language/en-GB/-ashok/cp.php?m=login	   |
		| http://vassallomotos.com.ar/tai/cp.php?m=login						   |
		| http://tkl-supplies.com.ar											   |
		| http://lion.web2.0campus.net/wp-admin/user/upload/cp.php?m=login		 |
		| http://rest-mlyn.com.ua/includes/db/server/cp.php?m=login				|
		| http://isolu.eu/phpb1/henry/uzo.php									  |
		| http://www.hostmyash.eu/za/update.php									|
		| http://sochicasflow.com/gboof/pictures/metro/admin/1/cp.php?letter=login |
		| http://199.201.121.177/neeew/server/root.php							 |
		| http://latiilots.co.uk/sp/gate.php									   |
		| http://biorecc.com/ern/gate.php										  |
		| http://latiilots.co.uk/sp/cp.php?m=login								 |
		| http://www.vozdocampoamapa.com.br/novo/libraries/_abdul/gate.php		 |
		| http://longgad.tk/company/gate.php									   |
		| http://charglosm.in/cgi/script/year/news/recent/z.php					|
		| http://wenden.itt21.de/metro/admin/1/secure.php						  |
		| http://petator:8080/analyzed/				|
		| http://www.pacificshipping.org/conf/gate.php							 |
		| http://www.vozdocampoamapa.com.br										|
		| http://mspeller.net/filing/update.php									|
		| http://wenden.itt21.de/metro/admin/1/cp.php?letter=login				 |
		| http://www.pacificshipping.org/conf/cp.php?m=login					   |
		| http://highlandsfm.org.au/images/pic/cp.php?m=login					  |
		| http://bijnortimes.com/image/pics/cp.php?m=login						 |
		| http://highlandsfm.org.au/images/pic/gate.php							|
		| http://keksdoseradio.com/image/logs/gate.php							 |
		| http://wenden.itt21.de												   |
	'''.splitlines()
	
	for u in testurls:
		u = re.sub(' *\| *','',u)
		oUrl = URL(u)
		print oUrl
		try:
			print 'getBase: [%s]'%oUrl.getBase()
			print 'getBaseDirectory: [%s]'%oUrl.getBaseDirectory()
			print 'getDirectory: [%s]'%oUrl.getDirectory()
			print 'getUrlDirectory:[%s]'%oUrl.getUrlDirectory()
		except:
			pass


def test_urls2dir():
	assert(URL().urls2dir('http://lala.com/a/b/c/shell','http://lala.com/a/gate')     == '../../')
	assert(URL().urls2dir('http://lala.com/a/b/c/shell','http://lala.com/a/b/gate')   == '../')
	assert(URL().urls2dir('http://lala.com/a/b/c/shell','http://lala.com/a/b/gate')   == '../')
	assert(URL().urls2dir('http://lala.com/a/b/c/shell','http://lala.com/a/b/c/gate') == './')


if __name__ == '__main__':
	#u = URL('http://susdu34568.com/cp.php?m=login')
	
	
	
	URL().urls2dir('http://aa.com/b//c', 'http://aa.com/b/c')
	
	
	
	
import re 
import os

class ConfigParser:
	def __init__(self,filename):
		self.comment = re.compile('^#')
		self.data = {}
		self.filename = filename
		self.load()

	def load(self):
		print 'loading petator from %s' % os.getcwd()
		fd = open(self.filename,'r')
		for d in fd.readlines():
			d = d.replace('\t',' ').replace(' ','').replace('\n','').replace('\r','')
			if not self.comment.match(d):
				if d.find(':') >= 0:
					k,v = d.split(':')
					if v == 'on':
						self.data[k] = True
					elif v == 'off':
						self.data[k] = False
					else:
						self.data[k]=v
		fd.close()

	def getConfig(self):
		return self.data


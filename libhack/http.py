import httplib2
import socket
import logging
import url


class HTTP:
    
    def __init__(self,timeout=20):
        self.timeout = timeout
        socket.setdefaulttimeout(timeout)
        self.http = httplib2.Http(timeout=self.timeout)
        self.headers = {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "text/plain",
            "User-agent": "Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0"
        }
        self.logger = logging.getLogger(self.__class__.__name__)
        self.html = ''
        self.hdrs = {}
        
    def setTimeout(self,timeout):
        self.timeout = timeout
        socket.setdefaulttimeout(timeout)
        
    def dirlistLookup(self,urlToScan):
        oUrl = url.URL(urlToScan)
        for oSuburl in oUrl.getSubUrls():
            if self.isDirList(url=oSuburl.getUrl()):
                return oSuburl.getUrl()
        return None
       
    def isDirList(self,url=None,html=None):
        if not url and not html:
            print 'method isDirList() bad called'
            return False
        
        if url:
            hdrs,html = self.get(url)
        
            if not hdrs or hdrs.status != 200:
                return False
        
        if 'index of' in html.lower():
            return True
        
        return False
        
    def check(self,url):
        try:
            self.hdrs,self.html = self.http.request(url, headers=self.headers, redirections=3)
            #print self.hdrs.status
            if self.hdrs.status != 404:
                return True
            else:
                return False
        except socket.timeout:
            return False
        except httplib2.RedirectLimit:
            return False
        except Exception, e:
            logging.error("[http] %s %s " % (e.message,e.args))
            return False
        
    def get(self,url):
        try:
            hdrs, html = self.http.request(url, 'GET', headers=self.headers, redirections=3)
            return hdrs, html
        except socket.timeout:
            return None, ''
        except httplib2.RedirectLimit:
            return None, ''
        except:
            return None, ''       
        
    def post(self,url,cmd):
        #TODO: refactor to return hdrs,html instead of html,hdrs (modify all xplts)
        try:
            cmd = cmd.replace(' ','%20')
            hdrs,html = self.http.request(url, 'POST', headers=self.headers, body=cmd, redirections=3)
            return html,hdrs
        except socket.timeout:
            return '', None
        except httplib2.RedirectLimit:
            return '', None
        except:
            return '', None
    
    def log(self,msg):  
        logging.info('[%s] %s' % (self.__class__.__name__,msg))


    def shellInjected(self,url,workspace):
        open(workspace+'/shellurl.log','w+').write(url)
        print '[shell]'


def test():
    #url = 'http://stackoverflow.com/questions/../2225038/python-determine-the-type-of-an-object'
    #url='http://recantodopastel.com.br/admin/../server/winserv_php_gate/test.php'
    url='http://www.4121.tv/go/server/../server/winserv_php_gate/test.php'
    h = HTTP()
    print h.check(url)
    




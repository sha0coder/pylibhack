import shell
from url import URL
import os

class Wget:
    
    def __init__(self):
        self.wget = shell.Shell('/usr/bin/wget')
        self.wget.debug = True
        self.params = " -U 'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 6.0)' "
        self.dir = ''
        self.ws = ''
        self.outfile = ''
        
    def setRecursive(self,limit=0):
        self.params += '-r -l %d ' % limit
        
    def setMirror(self):
        self.params += '-m '
        
    def setRetries(self,n):
        self.params += '-t %s ' % n
        
    def setContinue(self):
        self.params += '-c '
        
    def setUrl(self,url):
        self.params += url+' '
        self.dir = URL(url).hostport
        
    def setBaseUrl(self,url):
        self.params += '-B %s ' % url
        self.dir = URL(url).getDomain()
    
    def setNoRobots(self):
        self.params += '-e robots=off '
    
    def setOutFileName(self,filename):
        self.outfile = filename
        self.params += '-O '+filename+' '
    
    def setDestinationDir(self,directory):
        #self.params += '-P %s ' # % directory
        self.ws = directory
        
    def setDebug(self):
        print self.params
        
    def success(self):
        #TODO: check if mirror is used, if the directory is created
        if self.outfile:
            return os.path.exists(self.outfile)
        return False
         
    def run(self):
        prev = os.getcwd()
        os.chdir(self.ws)
        print '[wget] crawling ...'
        print '[wget params] '+self.params
        self.wget(self.params)
        os.chdir(prev)
        
        out = self.wget.out
        del out
        print '[wget] finished ...'
        
    def setTimeout(self,t):
        self.params = '-T --dns-timeout= --connect-timeout= --read-timeout='



'''
    by jo
'''

__bkp = {}
__whitelist = '''
    True False dict len range KeyboardInterrupt SyntaxError NameError abs print None ord RuntimeError MemoryError next str int OverflowError SystemExit 
'''.split()
__blacklist='''
    eval  
'''.split()
__safe = False

#eval dir __import__ reload open execfile  memoryview all file

def __setBuiltins(builtins):
    bi = range(len(builtins))
    __builtins__.__dict__.clear()
    for b in bi:
        __builtins__.__dict__[b] = builtins[b]


def paranoiaOn():
    p=a=r=a=n=o=i=a   =[]
    bad = []
    for b in __builtins__.__dict__.keys():
        __bkp.update({b:__builtins__.__dict__[b]})
        if b not in __whitelist:
            bad.append(b)
    for b in bad:
        __builtins__.__dict__.pop(b)


def __restore():
    if __bkp:
        for i in __bkp.keys():
            __builtins__.update({i:__bkp[i]})


def paranoiaOff():
    __restore()
    
def on():
    global __safe
    if __safe:
        return


    bad = []
    for b in __builtins__.keys():
        __bkp.update({b:__builtins__[b]})
        if b in __blacklist:
            bad.append(b)
    for b in bad:
        __builtins__.pop(b)
    __safe = True
    


def off():
    global __safe
    if not __safe:
        return
    __restore()
    __safe = False
    
def toString():
    print '---'
    for b in __builtins__.items():
        print b
    print '---'




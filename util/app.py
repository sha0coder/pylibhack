import sys
import time
import os


class App:
	def __init__(self,appname):
		self.appname = appname
		self.ready = False
		self.cbError = False
		self.showErrors = False
		self.db = None

	def init(self,cb):
		self.ready = cb(self)
		return self

	def showErrors(self):
		self.showErrors = True
		return self

	def hasDB(self,dbfile):
		self.db = Json(dbfile)
		self.db.load()
		return self

	def loop(self,cb,sleepTime):
		if not self.ready:
			print '[!!] Initialize first'
			sys.exit(1)

		try:
			print '[Ok] Daemon running'
			while True:
				print '[Ok] launching at '+str(datetime.datetime.utcnow())
				if not cb():
					print '[Ok] Process finished'
					return self
				time.sleep(sleepTime)

		except (KeyboardInterrupt, SystemExit):
			return self
		except Exception, e:
			if self.cbError:
				if not self.cbError(e):
					return self
			else:
				print e


	def ifErr(self,cb):
		self.cbError = cb
		return self





def doFirst(obj):
	obj.lala = 1
	return True

def onErr(e):
	print 'custom err: '+e
	return True

def daemon():
	print 'yeah'
	return True

#App('lala').init(doFirst).showErrors().ifErr(onErr).loop(daemon,10)




class Injector:
	def getAs(self,obj=None):
		obj.__init__(self) # reinicializa el objeto inyectado y lo retorna
		return obj


class Fichero(Injector):
	def __init__(self,filename=''):
		self.content = None
		self.filename = filename

	def read(self):
		self.content = open(self.filename).read()
		return self

	def readLines(self):
		content = open(self.filename).readLines()
		self.content = []
		for l in content:
			self.content.append()
		return self

	def append(self,line):
		self.content.append(line)
		return self

	def set(self,data):
		self.content = data
		return self

	def save(self):
		open(self.filename,'w').write(self.content)
		return self

	def unlink(self):
		os.unlink(self.filename)
		return self



class Data(Injector):
	def __init__(self,obj=None):
		self.db = None
		if obj:
			self.db = obj.content

	def display(self):
		print self.db
		return self

	def clear(self):
		self.db = None
		del self.db
		return self


f = Fichero('/tmp/test').set('hola').save()
f.getAs(Data()).display().clear().getAs(Fichero()).unlink()


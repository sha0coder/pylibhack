'''
    Basic convertions of python types
'''


def extract(data):
    
    if not data:
        return None
    
    elif type(data) is str:
        return data
    
    elif type(data) is int:
        return data
    
    elif type(data) is list:
        return extract(data[0])
    
    elif type(data) is tuple:
        return extract(data[0])
        
    elif type(data) is dict:
        return extract(data[data.keys()[0]])
        
    else:
        return data
        
        
def test():  
    print extract([1,2])
    print extract([[[1],2]])
    print extract(1)
    print extract('1')
    print extract(True)
    print extract([])

test()

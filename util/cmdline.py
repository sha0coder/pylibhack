import re
import sys


class CmdLine:
    
    def __init__(self,prompt,help):
        self.help = help
        self.prompt = prompt
        self.parse = re.compile('\(([a-zA-Z0-0]+)\)')
        self.cmds = self.parse.findall(help)
        
    def ask(self,msg):
        if msg:
            sys.stdout.write(msg)
            sys.stdout.flush()
        return sys.stdin.readline()[:-1]
        
    def h(self):
        print self.help
        
    def stop(self):
        self.isRunning = False
        
    def start(self):
        self.isRunning = True
        while self.isRunning:
            cmd = ''
            try:
                cmd = self.ask(self.prompt)
            except KeyboardInterrupt:
                sys.exit()
                
            if cmd in self.cmds:
                try:
                    eval('self.'+cmd+'()')
                except KeyboardInterrupt:
                    return 
                except AttributeError:
                    print 'command not implemented'
                except:
                    pass

            else:
                self.h()



class CMD(CmdLine):
    def x(self):
        sys.exit()
        
    def q(self):
        print 'queue'
        


def test():
    cmd = CMD('=>', '''
        (h) -> help
        (q) -> show queues
        (x) -> exit 
    ''')
    
    cmd.start()
    
    
    
